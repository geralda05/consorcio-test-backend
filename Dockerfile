# Construi la imagen pero después no supe como correrla en el contenedor y no tenia tiempo para dedicarme a investigar al respecto.

FROM node:12
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 8080
CMD [ "node", "app.js" ]