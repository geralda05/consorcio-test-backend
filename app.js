/* Inicialización */
var express = require("express");
var axios = require("axios");
var moment = require("moment-timezone");
var cors = require('cors');
app = express();
app.use(cors());
bodyParser  = require("body-parser");
methodOverride = require("method-override");
mongoose = require('mongoose');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

var router = express.Router();

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8888');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

/* Ruta principal */
router.get('/', function(req, res) {
   res.send("Para ver como utilizar la API diríjase a https://bitbucket.org/geralda05/consorcio-test-backend/src/master/");
});

/* Definición de mensajes de error */
const error500 = {success: false, message: 'Internal Server Error', status: 500};
const error400 = {success: false, message: 'Bad Request', status: 400};

/* Endpoint para filtrar por comuna y nombre (enviando el codigo de comuna) */
router.post('/farmacias', function(req, res) {

    /* Validación de datos recibidos en formato correcto */
    if(
        typeof req.body.comuna === 'string' && 
        typeof req.body.nombre === 'string' && 
        typeof req.body.id_region === 'number'
    ){
        try{
            /* Petición HTTP al servicio externo */
            axios({
                method:'get',
                url:'https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion',
                params:{
                    id_region: req.body.id_region,
                },
            }).then(response => {
                try{
                    /* Realizar el filtrado correspondiente por comuna y nombre */
                    const filtered = response.data.filter(x => x.fk_comuna === req.body.comuna).filter(x => (x.local_nombre.toLowerCase().includes(req.body.nombre.toLowerCase())));
                    res.header('Access-Control-Allow-Origin', '*');
                    res.status(200).send({success: true, data: filtered.map(x => {
                        /* Formatear correctamente los datos que deben ser recibidos */
                        return {
                            nombre: x.local_nombre, 
                            direccion: (x.local_direccion+', '+x.comuna_nombre), 
                            telefono: x.local_telefono, 
                            latitud: Number(x.local_lat), 
                            longitud: Number(x.local_lng), 
                            /* Añadi horario para habilitar la funcionalidad de ver si está abierta o cerrada al momento de consultar al front */
                            abierta: statusHora(x),
                        }
                    })});
                    res.end();
                }catch{
                    res.status(500).send(error500);
                    res.end();            
                }
            })
        }catch{
            res.status(500).send(error500);
            res.end();            
        }
    }else{
        res.status(400).send(error400);
        res.end();
    }
 });

 /* Endpoint para filtrar por comuna y nombre (enviando el nombre de la comuna) */
 router.post('/farmacia-comuna', function(req, res) {

    /* Validación de datos recibidos en formato correcto */
    if(
        typeof req.body.comuna === 'string' && 
        typeof req.body.nombre === 'string' && 
        typeof req.body.id_region === 'number'
    ){
        try{

            /* Petición HTTP al servicio externo */
            axios({
                method:'get',
                url:'https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion',
                params:{
                    id_region: req.body.id_region,
                },
            }).then(response => {
                try{
                    /* Realizar el filtrado correspondiente por comuna y nombre */
                    const filtered = response.data.filter(x => x.comuna_nombre.toLowerCase().includes(req.body.comuna.toLowerCase())).filter(x => (x.local_nombre.toLowerCase().includes(req.body.nombre.toLowerCase())));
                    res.header('Access-Control-Allow-Origin', '*');
                    res.status(200).send({success: true, data: filtered.map(x => {
                        /* Formatear correctamente los datos que deben ser recibidos */
                        return {
                            nombre: x.local_nombre, 
                            direccion: (x.local_direccion+', '+x.comuna_nombre), 
                            telefono: x.local_telefono, 
                            latitud: Number(x.local_lat), 
                            longitud: Number(x.local_lng), 
                            /* Añadi horario para habilitar la funcionalidad de ver si está abierta o cerrada al momento de consultar al front */
                            abierta: statusHora(x),
                        }
                    })});
                    res.end();
                }catch{
                    res.status(500).send(error500);
                    res.end();            
                }
            })
        }catch{
            res.status(500).send(error500);
            res.end();            
        }
    }else{
        res.status(400).send(error400);
        res.end();
    }
 });

 function statusHora(x){
     /************
      * FUNCION PARA DETERMINAR SI LA FARMACIA ESTÁ ABIERTA AL MOMENTO DE HACER LA CONSULTA UTILIZANDO LA HORA DE APERTURA Y CIERRE DE LA MISMA
      */
    const hora_separada = x.funcionamiento_hora_apertura.split(' ')[0].split(':');
    const hora_cierre = x.funcionamiento_hora_cierre.split(' ')[0].split(':');
    const apertura = moment().tz('America/Santiago').set({hour: hora_separada[0],minute:hora_separada[1],second:0,millisecond:0}); 
    const cierre = moment().tz('America/Santiago').set({hour: hora_cierre[0],minute:hora_cierre[1],second:0,millisecond:0}); 
    const actual = moment().tz('America/Santiago');
    if(apertura.valueOf() <= actual.valueOf() && actual.valueOf() < cierre.valueOf()){
        return true
    }else{
        return false
    }
 }

app.use(router);

const port = process.env.PORT || 8080;
app.listen(port, function() {
  console.log("Node server running on http://localhost:8080");
});