# API REST for ConsorcioJobTest
### Made by Gerald Leonel Alarcon
### Generated with NodeJS and ExpressJS

## URL and Web Ubication

Available in `http://consorcio-test-backend.herokuapp.com`

## Development server

Run `node app.js` for a dev server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.

## Peticiones disponibles:

### POST /farmacias (filtrado de farmacias enviando el codigo de comuna)
### BODY:
```
{
    "comuna": "String",
    "nombre": "String",
    "id_region": "Number"
}
```
### PETICIÓN RETORNA ARRAY DE FARMACIAS EN FORMATO
```
{
    "nombre": "Nombre",
    "direccion": "Direccion, Comuna",
    "telefono": "Telefono",
    "latitud": 0000000,
    "longitud": 0000000,
    "abierta": true
}
```

### POST /farmacia-comuna (filtrado de farmacias enviando el nombre de la comuna)
### BODY:
```
{
    "comuna": "String",
    "nombre": "String",
    "id_region": "Number"
}
```


